﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nag.Users.Core.DTOs;
using Nag.Users.Core.Models;
using Nag.Users.Data;

namespace Nag.Orders.Service.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : ControllerBase
    {
        public MySqlDataContext Context { get; }

        public OrdersController(MySqlDataContext context)
        {
            Context = context;
        }
      
        // GET api/order/5
        [HttpGet("{userid}")]
        public IEnumerable<OrderDto> Get(int userid)
        {
            return this.Context.Orders.Where(x=> x.UserId == userid).ToList().Select(x => new OrderDto() {  Id = x.Id, Ammount = x.Amount, Date = x.Date }).ToList();
        }

        // POST api/order
        [HttpPost]
        public void Post([FromBody] OrderDto x)
        {
            this.Context.Orders.Add(new Order() { Amount = x.Ammount, UserId = x.UserId, Date = x.Date });
            this.Context.SaveChanges();
        }

        // DELETE api/order/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var Order = this.Context.Orders.FirstOrDefault(x => x.Id == id);
            if (Order != null)
            {
                this.Context.Orders.Remove(Order);
                this.Context.SaveChanges();
            }
        }
    }
}
